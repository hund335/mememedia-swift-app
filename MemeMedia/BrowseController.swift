//
//  swift
//  MemeMedia
//
//  Created by Frederik Højgaard on 11/07/2017.
//  Copyright © 2017 Frederik Højgaard. All rights reserved.
//

import UIKit

class BrowseController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var posters = [String]()
    var titles = [String]()
    var photos = [String]()
    var dates = [String]()
    var ids = [String]()
    
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let img = UIImageView(image: #imageLiteral(resourceName: "background-browse"))
        table.backgroundView = img
       
        table.delegate = self
        table.dataSource = self
        posters.removeAll()
        titles.removeAll()
        photos.removeAll()
        dates.removeAll()
        ids.removeAll()
        self.getMemes()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        

    }
    
    
    @IBAction func profileButton(_ sender: UIButton) {
        
    }
    
    
    @IBAction func logoffButton(_ sender: UIButton) {
        let ap = api.init()
        ap.logOff(location: "browseLogoff", controller: self)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    @IBAction func profileClick(_ sender: UIButton) {
        
        api.profile = sender.currentTitle!
        self.performSegue(withIdentifier: "toOtherProfile", sender: nil)
    }
    
    
    @IBAction func commentsClick(_ sender: UIButton) {
        let cell = sender.superview?.superview as! TableViewCell
        let row = table.indexPath(for: cell)
        api.id = Int(ids[(row?.row)!])
        api.poster = posters[(row?.row)!]
        api.image = Int(photos[(row?.row)!])
        api.title = titles[(row?.row)!]
        api.date = dates[(row?.row)!]
        self.performSegue(withIdentifier: "toComments", sender: nil)
        
        
    }
    
    
    
    func getMemes(){
    
        let url = URLRequest(url: URL(string: "http://minehub.dk/login/allmemes.php?user=\(ViewController.getUser())&token=\(ViewController.getToken())")!)
        
       
        let session = URLSession.shared.dataTask(with: url){ data, response, error in
            guard let data = data, error == nil else{
            //Network errors here
                
            return
            }
           
            var convert = try! JSONSerialization.jsonObject(with: data) as! NSArray
            
            if convert.description.contains("poster") == false{
                
                let ap = api.init()
                ap.invalidSession(controller: self, loc: "browseLogoff", data: data)
                return
            }
        
            
            let getPosters = convert.value(forKey: "poster") as! NSArray
            let getIds = convert.value(forKey: "id") as! NSArray
            let getTitles = convert.value(forKey: "title") as! NSArray
            let getPhotos = convert.value(forKey: "photo") as! NSArray
            let getDates = convert.value(forKey: "date") as! NSArray

            
            getTitles.forEach(){get in
                self.titles.append(get as! String)
            }
           
            getIds.forEach(){get in
                self.ids.append(get as! String)
            }
            
            getPosters.forEach(){get in
                self.posters.append(get as! String)
            }
            
            getPhotos.forEach(){get in
                self.photos.append(get as! String)
            }
            
            getDates.forEach(){get in
                self.dates.append(get as! String)
            }
            
            DispatchQueue.main.async {
                self.table.reloadData()
            }
            
        }
        session.resume()
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return posters.count
        
        
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Memes", for: indexPath) as! TableViewCell
        
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.backgroundColor = UIColor.clear
            tableView.separatorStyle = .none
        
            cell.title.text = titles[indexPath.row] as! String
            cell.poster.setTitle(posters[indexPath.row] as! String, for: .normal)
            cell.date.text = dates[indexPath.row] as!String
        
        if(cell.imageLabel.image == nil){
        
        DispatchQueue.main.async {
            
        
            let imgUrl = URL(string: "http://minehub.dk/login/memes/pictures/\(self.photos[indexPath.row]).png")!
            let image = try! Data(contentsOf: (imgUrl as URL))
            cell.imageLabel.image = UIImage(data: image)
        }
        }
        
        return cell;
            
       
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
