//
//  CommentsController.swift
//  MemeMedia
//
//  Created by Frederik Højgaard on 11/08/2017.
//  Copyright © 2017 Frederik Højgaard. All rights reserved.
//

import UIKit

class CommentsController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var poster: UIButton!
    @IBOutlet weak var postDate: UILabel!

    var getPosters = [String]()
    var getComments = [String]()
    var getDates = [String]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        postTitle.text = api.title
        poster.setTitle(api.poster, for: .normal)
        postDate.text = api.date
      
        getPosters.removeAll()
        getComments.removeAll()
        getDates.removeAll()
        loadComments()
        
        if postImage.image == nil{
            DispatchQueue.main.async {
                let data = try! Data.init(contentsOf: URL(string: "http://minehub.dk/login/memes/pictures/\(api.image as! Int).png")!)
                self.postImage.image = UIImage(data: data)
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func loadComments(){
        let url = URLRequest(url: URL(string: "http://minehub.dk/login/comments.php?user=\(ViewController.getUser())&token=\(ViewController.getToken())&id=\(api.id as! Int)")!)
        let session = URLSession.shared.dataTask(with: url){ data, response, error in
            guard let data = data, error == nil else{
                return
            }
            var convert = try! JSONSerialization.jsonObject(with: data) as! NSArray
            
            if !convert.description.contains("poster"){
                return
            }
            
            let tPosters = convert.value(forKey: "poster") as! NSArray
            let tComments = convert.value(forKey: "comment") as! NSArray
            let tDates = convert.value(forKey: "date") as! NSArray
            
            tPosters.forEach({get in
                self.getPosters.append(get as! String)
            })
            
            tComments.forEach({get in
                self.getComments.append(get as! String)
            })
            
            tDates.forEach({get in
                self.getDates.append(get as! String)
            })
            
        
        }
        session.resume()
        
    
    }
    

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    
        return getPosters.count;
    }
    
    

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "Comments", for: indexPath) as! CommentsViewCell
        cell.poster.setTitle(getPosters[indexPath.row], for: .normal)
        cell.comment.text = getComments[indexPath.row]
        cell.date.text = getDates[indexPath.row]
        
        return cell;
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
