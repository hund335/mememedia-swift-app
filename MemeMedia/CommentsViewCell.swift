//
//  CommentsViewCell.swift
//  MemeMedia
//
//  Created by Frederik Højgaard on 11/08/2017.
//  Copyright © 2017 Frederik Højgaard. All rights reserved.
//

import UIKit

class CommentsViewCell: UITableViewCell {

    @IBOutlet weak var poster: UIButton!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var date: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
