//
//  OtherProfileController.swift
//  MemeMedia
//
//  Created by Frederik Højgaard on 20/07/2017.
//  Copyright © 2017 Frederik Højgaard. All rights reserved.
//

import UIKit

class OtherProfileController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var getProfile: UILabel!
    @IBOutlet weak var totalStars: UILabel!
    @IBOutlet weak var totalMemes: UILabel!
    @IBOutlet weak var totalPosts: UILabel!
    @IBOutlet weak var table: UITableView!
    
    var getTitles = [String]()
    var getPosters = [String]()
    var getPhotos = [String]()
    var getDates = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        getProfile.text = "\(api.profile as! String)´s profile"
       
        self.getMemes()
        table.delegate = self
        table.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func homeButton(_ sender: UIButton) {
        let ap = api.init()
        ap.gotoHome(location: "backOtherProfile", controller: self)
    }
    
    @IBAction func profileButton(_ sender: UIButton) {
        let ap = api.init()
    }

    @IBAction func logoffButton(_ sender: UIButton) {
        let ap = api.init()
        ap.logOff(location: "otherProfileLogoff", controller: self)
    }

    
    
    public func getMemes(){
        
        let url = URLRequest(url: URL(string: "http://minehub.dk/login/allmemesby1.php?target=\(api.profile as! String)&user=\(ViewController.getUser())&token=\(ViewController.getToken())")!)
        
        let session = URLSession.shared.dataTask(with: url) { data, repsonse, error in
            guard let data = data, error == nil else{
            //network errors
                return;
            }
    
        let convert = try! JSONSerialization.jsonObject(with: data) as! NSArray
            
            if !convert.description.contains("poster"){
                let ap = api.init()
                ap.invalidSession(controller: self, loc: "otherProfileLogoff", data: data)
                return
            }
            
            let tryTitles = convert.value(forKey: "title") as! NSArray
            let tryPosters = convert.value(forKey: "poster") as! NSArray
            let tryPhotos = convert.value(forKey: "photo") as! NSArray
            let tryDates = convert.value(forKey: "date") as! NSArray
            
            tryTitles.forEach(){get in
                self.getTitles.append(get as! String)
            }
            
            tryPosters.forEach(){get in
                self.getPosters.append(get as! String)
            }
            
            tryPhotos.forEach(){get in
                self.getPhotos.append(get as! String)
            }
            
            tryDates.forEach(){get in
                self.getDates.append(get as! String)
            }
            
            DispatchQueue.main.async {
                self.table.reloadData()
            }
    }
        session.resume()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getTitles.count
        
        
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "otherProfile", for: indexPath) as! OtherProfileVievCell
        cell.title.text = self.getTitles[indexPath.row]
        cell.poster.text = self.getPosters[indexPath.row]
        cell.date.text = self.getDates[indexPath.row]
        
        if(cell.memeImage.image == nil){
            DispatchQueue.main.async {
                
                let url = URL(string: "http://minehub.dk/login/memes/pictures/\(self.getPhotos[indexPath.row]).png")
                let image = try! NSData(contentsOf: url!)
    
                cell.memeImage.image = UIImage(data: image as! Data)
            }
        }
        
        return (cell)
        }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
