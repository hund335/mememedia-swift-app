//
//  OtherProfileVievCell.swift
//  MemeMedia
//
//  Created by Frederik Højgaard on 20/07/2017.
//  Copyright © 2017 Frederik Højgaard. All rights reserved.
//

import UIKit

class OtherProfileVievCell: UITableViewCell {

    @IBOutlet weak var memeImage: UIImageView!
    @IBOutlet weak var poster: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
