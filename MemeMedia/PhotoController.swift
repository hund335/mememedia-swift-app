//
//  PhotoController.swift
//  MemeMedia
//
//  Created by Frederik Højgaard on 16/07/2017.
//  Copyright © 2017 Frederik Højgaard. All rights reserved.
//

import UIKit

class PhotoController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var titleLabel: UITextField!
    @IBOutlet weak var imageField: UIImageView!
    @IBOutlet var uploadBar: UIView!
    @IBOutlet weak var procentLabel: UILabel!
    @IBOutlet weak var uploadButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func uploadEvent(_ sender: UIButton) {
        
        print("Upload event")
        
        let getImages = UIImagePickerController()
        getImages.delegate = self
        getImages.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.presentedViewController?.present(getImages, animated: true, completion: nil)
        
    }
 
    

    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        imageField.image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageField.backgroundColor = UIColor.clear
        self.dismiss(animated: true, completion: nil)
        
    }
   
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
