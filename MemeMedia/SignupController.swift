//
//  SignupController.swift
//  MemeMedia
//
//  Created by Frederik Højgaard on 04/07/2017.
//  Copyright © 2017 Frederik Højgaard. All rights reserved.
//

import UIKit

class SignupController: UIViewController {

    @IBOutlet weak var userSignup: UITextField!
    @IBOutlet weak var emailSignup: UITextField!
    @IBOutlet weak var passSignup: UITextField!
    @IBOutlet weak var confpassSignup: UITextField!
    @IBOutlet weak var bdaySignup: UIDatePicker!
    let ap = api.init()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signupButton(_ sender: UIButton) {
        
        if(self.userSignup.text?.isEmpty == true || self.emailSignup.text?.isEmpty == true || self.passSignup.text?.isEmpty == true || self.confpassSignup.text?.isEmpty == true){
            self.ap.pushMsg(message: "Be sure that none of the fields are empty!", title: "Error", controller: self)
            
        }else{
        
            var bday = DateFormatter()
            bday.dateFormat = "dd-MM-yyy"
            
            var url = URLRequest(url: URL(string: "http://minehub.dk/login/register.php?user=\(userSignup.text as! String)&email=\(emailSignup.text as! String)&password=\(passSignup.text as! String)&confirmpassword=\(confpassSignup.text as! String)&birtday=\(bday.string(from: bdaySignup.date))")!)
                url.httpMethod = "POST"
   
            let session = URLSession.shared.dataTask(with: url){ data, response, error in guard let data = data, error == nil else{
                self.ap.pushMsgAsync(message: "Couldn't connect to the server!", title: "Error", controller: self)
                return
                }
                
                if(self.ap.getStatus(data: data) != "Success"){
                
                    self.ap.pushMsgAsync(message: self.ap.getMessage(data: data), title: "Error", controller: self)
                    return;
                }else{
                    self.ap.pushMsgAsync(message: self.ap.getMessage(data: data), title: "Success", controller: self)
                }
        
        }
        session.resume()
    }
}
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
