//
//  TableViewCell.swift
//  MemeMedia
//
//  Created by Frederik Højgaard on 11/07/2017.
//  Copyright © 2017 Frederik Højgaard. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var imageLabel: UIImageView!
    @IBOutlet weak var poster: UIButton!
    @IBOutlet weak var comments: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
