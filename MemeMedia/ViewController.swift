//
//  ViewController.swift
//  LommeRegner
//
//  Created by Frederik Højgaard on 28/06/2017.
//  Copyright © 2017 Frederik Højgaard. All rights 
//  reserved.



import UIKit
import Foundation

class ViewController: UIViewController {
    
    @IBOutlet weak var emailFront: UITextField!
    @IBOutlet weak var passFront: UITextField!
    private static var gUser:String!
    private static var gToken:String!
    
    let ap = api.init()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

    }
    
    override func viewDidAppear(_ animated: Bool) {
        if api.valid == false{
            let ap = api.init()
            ap.pushMsg(message: api.gMessage, title: api.gTitle, controller: self)
            api.valid = true
    }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        
        if(emailFront.text?.isEmpty == true || passFront.text?.isEmpty == true) {
            let push = UIAlertController.init(title: "Error", message: "Either are the email or password field empty", preferredStyle: .alert)
            push.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: nil))

            self.present(push, animated: true, completion: nil)
        
        }else{
            var url = URLRequest(url: URL(string: "http://minehub.dk/login/login.php?email=\(emailFront.text as! String)&password=\(passFront.text as! String)")!)
            
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                self.ap.pushMsgAsync(message: "Couldn't connect to the server!", title: "Error", controller: self)
                    return
                    }
                
                if(self.ap.getStatus(data: data) == "Success"){
                    DispatchQueue.main.async {
                       let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSDictionary
                        ViewController.gUser = json["user"] as! String
                        ViewController.gToken = json["session"] as! String
                        self.performSegue(withIdentifier: "Login", sender: nil)
                        }
                        
                    }else{
                       // self.ap.pushMsgAsync(message: "Email and password do not match!", title: "Error", controller: self)
                        self.ap.pushMsgAsync(message: self.ap.getMessage(data: data), title: self.ap.getStatus(data: data), controller: self)
                        }
                
                }
                task.resume()
        
        }
    }
    
    public static func getUser() -> String{
    
        return gUser
    }
    
    public static func clearUser(){
        gUser = ""
    }
    
    public static func getToken() -> String{
    
        return gToken
    }
    
    public static func clearToken(){
        gToken = ""
    }
}

