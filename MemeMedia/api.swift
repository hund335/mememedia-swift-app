//
//  api.swift
//  MemeMedia
//
//  Created by Frederik Højgaard on 09/07/2017.
//  Copyright © 2017 Frederik Højgaard. All rights reserved.
//

import Foundation
import UIKit

public class api:UIViewController {
    
    
    //GOTO PROFILE
    static var profile:String!
    
    //POST INFO
    static var id:Int!
    static var poster:String!
    static var image:Int!
    static var title:String!
    static var date:String!
    
    
    static var valid:Bool = true
    static var gMessage:String!
    static var gTitle:String!
    
    func pushMsg(message:String, title:String, controller:UIViewController){
        let push = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        push.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
        controller.present(push, animated: true, completion: nil)
}
    
    func pushMsgAsync(message:String, title:String, controller:UIViewController){
        DispatchQueue.main.async {
            let push = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
            push.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            controller.present(push, animated: true, completion: nil)
        }
    }
    
    
    func gotoHome(location:String, controller:UIViewController){
        controller.performSegue(withIdentifier: location, sender: nil)
    }
    
    func logOff(location:String, controller:UIViewController){
        
        ViewController.clearUser()
        ViewController.clearToken()
        controller.performSegue(withIdentifier: location, sender: nil)
    }
    
    
    
    func getStatus(data:Data) -> String{
        
        let folder:NSDictionary = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary as! NSDictionary
        let message = folder["status"]
        return message as! String
        
    }
    
    
    
    func invalidSession(controller:UIViewController, loc:String, data:Data){
        
        let convert = try! JSONSerialization.jsonObject(with: data) as! NSArray
        let title = convert.value(forKey: "status") as! NSArray
        let message = convert.value(forKey: "message") as! NSArray
        api.gMessage = message[0] as! String
        api.gTitle = title[0] as! String
        
            DispatchQueue.main.async {
                api.valid = false
                controller.performSegue(withIdentifier: loc, sender: nil)
                ViewController.clearUser()
                ViewController.clearToken()
                
        }
        
    }

    
    func convertData(data:Data, type:String){

        
        do {
                let json = try JSONSerialization.jsonObject(with: data) as? NSArray
            

        
            for getRes in json?.value(forKey: "title") as! NSArray{
             
            }
        
        } catch {
            print("Error deserializing JSON: \(error)")
        }

    }
    
    func getMessage(data:Data) -> String{
        
        let folder:NSDictionary = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary as! NSDictionary
        let message = folder["message"]
        return message as! String
        
    }
    
}
